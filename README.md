# 2018 Rumble in the Roads Powercube Tracking
## Author(s): Naren Kasinath
## Overview
The following project seeks to create a stable and reliable algorithm to track power cubes in autonomous for the 2018 FIRST Power Up Rumble in the Roads Competition. To track the cube, a system using HSV color filters and Canny Edge Detection was created using OpenCV and Python.
## Libraries Needed
- OpenCV (Python)
- Numpy
- Socket

## Hardware Needed
- Single Board Computer with a GNU/Linux OS and Python installed
	- High Resolution USB Camera (For Vision Processing)
	- Ethernet Cable and Network Bridge (For RoboRIO Communication)
- 2018 FIRST Power Up Powercube (For Haar Cascade Training)
	- High Resolution Camera (For Photographing the Cube)
- Quad Core Desktop Computer with OpenCV installed (To train the Haar Cascade)

## Additional Documentation Links
- OpenCV Docs: https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html
- Python Docs: https://docs.python.org/3/