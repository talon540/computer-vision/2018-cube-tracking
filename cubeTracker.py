import cv2
import numpy as np
import time as t
import socket
import sys

#HSV color: (29, 207, 74)
HSV_LOWER_BOUND = [25, 90, 90]
HSV_UPPER_BOUND = [35, 255, 255]

CANNY_MINVAL = 50
CANNY_MAXVAL = 70

UDP_IP = '10.5.40.1'
UDP_PORT = '5805'

cap = cv2.VideoCapture(0)
#cap = cv2.VideoCapture(2)
ret, frame = cap.read()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def HSV(src, lowerBound, upperBound): 
    #frame = cv2.imread('cube.png')
    src = cv2.resize(src, (500,500))
    
    hsv = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)
    lower = np.array(lowerBound)
    upper = np.array(upperBound)
    mask = cv2.inRange(hsv, lower, upper)
    
    res = cv2.bitwise_and(src, src, mask=mask)

    #cv2.imshow('mask', mask)
    #cv2.imshow('res', res)
    return res
        	
def findContours(src, minVal, maxVal):
    blur = cv2.GaussianBlur(src, (5,5), 0)
    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
        
    sobelX = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=5)
    sobelY = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=5)
    sobel = cv2.addWeighted(cv2.convertScaleAbs(sobelX), 0.5, cv2.convertScaleAbs(sobelY), 0.5, 0)

    edges = cv2.Canny(sobel, minVal, maxVal)
    _, contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if (len(contours) > 0):
        cnt = max(contours, key=cv2.contourArea)
    else:
        cnt = None
    
    #cv2.imshow('blur', blur)
    #cv2.imshow('sobel', sobel)
    #cv2.imshow('edges', edges)

    return cnt

def drawBoundingRect(src, contour):
    if(cv2.contourArea(contour) > 8000):
        x,y,w,h = cv2.boundingRect(contour)
        src = cv2.rectangle(src, (x,y), (x+w,y+h), (255,255,255), 3)
        ctr = (x+(w/2),y+(h/2))
    else:
        ctr = None
    
    #cv2.imshow('Frame', frame)
    
    return ctr
        
def targetTrack(target):
    if(target[0] > 260):
        shift = 'R'
    elif(target[0] < 240):
        shift = 'L'
    else:
        shift = 'C'
    return shift
        
def sendData(val, targetIP, port):
    sock.sendto(val, (targetIP, port))
        
def terminate():
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()

def main():
    if(ret):
        hsv = HSV(frame, HSV_LOWER_BOUND, HSV_UPPER_BOUND)
        cnt = findContours(hsv, CANNY_MINVAL, CANNY_MAXVAL)
        if(cnt is None):
            print("No contours found")
        else:
            ctrPoint = drawBoundingRect(frame, cnt)
            if(ctrPoint is None):
                print("No significant targets found")
            else:
                print(str(ctrPoint))
                alignment = targetTrack(ctrPoint)
                #sendData(alignment, UDP_IP, UDP_PORT)

if(__name__ == "__main__"):
    while(True):
        main()
        t.sleep(0.01)
        
        '''
        key = cv2.waitKey(1)
        if(key == ord('q')):
            terminate()
        '''
